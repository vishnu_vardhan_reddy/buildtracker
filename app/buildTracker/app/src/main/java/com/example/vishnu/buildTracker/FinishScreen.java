package com.example.vishnu.buildTracker;




import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.vishnu.buildthem.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by vishnu on 12/30/15.
 */
public class FinishScreen extends Activity{

    String url = "";

    private EditText familyAndType;
    private EditText area;
    private EditText cost;
    private EditText materialOrderDate;
    private EditText materialOrderStatus;
    private EditText materialDeliveryDate;
    private EditText materialDeliveryStatus;
    private EditText materialLocation;
    private EditText itemCost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finish);

        //asked you to change these names na.change it now
        /*final TextView table =(TextView)findViewById(R.id.textView);
        final TextView table2 =(TextView)findViewById(R.id.textView2);
        final TextView table3 =(TextView)findViewById(R.id.textView3);
        final TextView table4 =(TextView)findViewById(R.id.textView4);
        final TextView table5 =(TextView)findViewById(R.id.textView5);
        final TextView table6 =(TextView)findViewById(R.id.textView6);
        final TextView table7 =(TextView)findViewById(R.id.textView7);
        final TextView table8 =(TextView)findViewById(R.id.textView8);
        final TextView table9 =(TextView)findViewById(R.id.textView9);*/

        familyAndType =(EditText) findViewById(R.id.editText);
        area =(EditText) findViewById(R.id.editText2);
        cost =(EditText) findViewById(R.id.editText3);
        materialOrderDate =(EditText) findViewById(R.id.editText4);
        materialOrderStatus=(EditText) findViewById(R.id.editText5);
        materialDeliveryDate =(EditText) findViewById(R.id.editText6);
        materialDeliveryStatus =(EditText) findViewById(R.id.editText7);
        materialLocation =(EditText) findViewById(R.id.editText8);
        itemCost =(EditText) findViewById(R.id.editText9);

        url = "http://"+getIntent().getStringExtra("url");
        //String url ="http://www.mocky.io/v2/568956b7120000920d1f3fde";
        JsonObjectRequest request = new JsonObjectRequest(url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        //response message
                        try {
                            //if response.string == success check // TODO: 1/3/16
                            JSONObject materialDetailsObj = response.getJSONObject("materialDetails");
                            Map<String,Object> valueByKey = JsonUtils.jsonObjectToMap(materialDetailsObj);
                            MaterialDetails materialDetails = new MaterialDetails();
                            materialDetails.setValueByKey(valueByKey);
                            familyAndType.setText(valueByKey.get("familyAndType").toString());
                            area.setText(valueByKey.get("area").toString());
                            cost.setText(valueByKey.get("cost").toString());
                            materialOrderDate.setText(valueByKey.get("materialOrderDate").toString());
                            materialOrderStatus.setText(valueByKey.get("materialOrderStatus").toString());
                            materialDeliveryDate.setText(valueByKey.get("materialDeliveryDate").toString());
                            materialDeliveryStatus.setText(valueByKey.get("materialDeliveryStatus").toString());
                            materialLocation.setText(valueByKey.get("materialLocation").toString());
                            itemCost.setText(valueByKey.get("itemCost").toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },

                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //error message
                    }
                }
        );
        VolleyApplication.getInstance().getRequestQueue().add(request);


    }


    public void cancel(View v) {
        Intent intent = new Intent(FinishScreen.this,MainActivity.class);
        startActivity(intent);
    }


    public void save(View v) {

        StringRequest putRequest = new StringRequest(Request.Method.PUT, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //TODO check if response is a success or not
                        Log.d("Response", response);
                        if (response.contains("success")) {
                            Toast toast = Toast.makeText(FinishScreen.this, "Your changes have been saved successfully!" , Toast.LENGTH_LONG);
                            toast.show();
                        } else {
                            Toast errorToast = Toast.makeText(FinishScreen.this,"Oops! Seems like there's an error. Try again later. " +
                                    "If the error still persists, contact your admin.", Toast.LENGTH_LONG);
                            errorToast.show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        Toast errorToast = Toast.makeText(FinishScreen.this,"Oops! Seems like there's an error. Try again later. " +
                                "If the error still persists, contact your admin.", Toast.LENGTH_LONG);
                        errorToast.show();
                        Log.d("Error.Response", error.toString());
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() {
                //TODO form methods to send.
                Map<String, String>  params = new HashMap<String, String>();
                params.put("familyAndType", familyAndType.getText().toString());
                params.put("area", area.getText().toString());
                params.put("cost", cost.getText().toString());
                params.put("materialOrderDate", materialOrderDate.getText().toString());
                params.put("materialOrderStatus", materialOrderStatus.getText().toString());
                params.put("materialDeliveryDate", materialDeliveryDate.getText().toString());
                params.put("materialDeliveryStatus", materialDeliveryStatus.getText().toString());
                params.put("materialLocation", materialLocation.getText().toString());
                params.put("itemCost", itemCost.getText().toString());
                return params;
            }
        };

        VolleyApplication.getInstance().getRequestQueue().add(putRequest);
    }

}



