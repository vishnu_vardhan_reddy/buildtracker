package com.example.vishnu.buildTracker;

import java.util.Map;
/**
 * Created by vishnu on 1/3/16.
 */
//material details model class
public class MaterialDetails {

    private Map<String,Object> valueByKey;

    public Map<String,Object> getValueByKey() {
        return valueByKey;
    }
    public void setValueByKey(Map<String,Object> valueByKey) {
        this.valueByKey = valueByKey;
    }

    /*private Integer materialId;

    private String barcode;

    private String code;

    private String familyAndType;

    private String area;

    private String cost;

    private String materialOrderDate;

    private String materialOrderStatus;

    private String materialDeliveryDate;

    private String materialDeliveryStatus;

    private String materialLocation;

    private String itemCost;

    public MaterialDetails() {

    }

    public MaterialDetails(Integer materialId,String barcode, String code, String familyAndType, String area, String cost,
                           String materialOrderDate,String materialOrderStatus,String materialDeliveryDate,String materialDeliveryStatus,
                           String materialLocation,String itemCost) {
        this.materialId = materialId;
        this.barcode= barcode;
        this. code =  code;
        this.familyAndType = familyAndType;
        this.area = area;
        this.cost = cost;
        this.materialOrderDate = materialOrderDate;
        this.materialOrderStatus = materialOrderStatus;
        this.materialDeliveryDate = materialDeliveryDate;
        this.materialDeliveryStatus = materialDeliveryStatus;
        this.materialLocation = materialLocation;
        this.itemCost = itemCost;
    }

    public Integer getMaterialId() {
        return materialId;
    }

    public void setMaterialId(Integer materialId) {
        this.materialId = materialId;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFamilyAndType() {
        return familyAndType;
    }

    public void setFamilyAndType(String familyAndType) {
        this.familyAndType = familyAndType;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getMaterialOrderDate() {
        return materialOrderDate;
    }

    public void setMaterialOrderDate(String materialOrderDate) {
        this.materialOrderDate = materialOrderDate;
    }

    public String getMaterialOrderStatus() {
        return materialOrderStatus;
    }

    public void setMaterialOrderStatus(String materialOrderStatus) {
        this.materialOrderStatus = materialOrderStatus;
    }

    public String getMaterialDeliveryDate() {
        return materialDeliveryDate;
    }

    public void setMaterialDeliveryDate(String materialDeliveryDate) {
        this.materialDeliveryDate = materialDeliveryDate;
    }

    public String getMaterialDeliveryStatus() {
        return materialDeliveryStatus;
    }

    public void setMaterialDeliveryStatus(String materialDeliveryStatus) {
        this.materialDeliveryStatus = materialDeliveryStatus;
    }

    public String getMaterialLocation() {
        return materialLocation;
    }

    public void setMaterialLocation(String materialLocation) {
        this.materialLocation = materialLocation;
    }

    public String getItemCost() {
        return itemCost;
    }

    public void setItemCost(String itemCost) {
        this.itemCost = itemCost;
    }*/
}
