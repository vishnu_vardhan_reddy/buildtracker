package com.example.vishnu.buildTracker;

/**
 * Created by vishnu on 1/3/16.
 */

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonUtils {

    public static Map<String, Object> jsonObjectToMap(JSONObject jsonObject) {

        if(jsonObject.length() == 0) {
            return Collections.emptyMap();
        }
        Map<String, Object> valueByKey = new HashMap<String, Object>();
        for(int index = 0; index<jsonObject.names().length(); index++) {
            String key;
            try {
                key = jsonObject.names().getString(index);
                Object value = jsonObject.get(jsonObject.names().getString(index));
                valueByKey.put(key, value);
            } catch (JSONException e) {
                //handle error
                return valueByKey;
            }
        }
        return valueByKey;
    }

    public static Map<String, String> jsonObjectToStringMap(JSONObject jsonObject) {

        if(jsonObject.length() == 0) {
            return Collections.emptyMap();
        }
        Map<String, String> valueByKey = new HashMap<String, String>();
        for(int index = 0; index<jsonObject.names().length(); index++) {
            String key;
            try {
                key = jsonObject.names().getString(index);
                String value = jsonObject.get(jsonObject.names().getString(index)).toString();
                valueByKey.put(key, value);
            } catch (JSONException e) {
                //handle error
                return valueByKey;
            }
        }
        return valueByKey;
    }

    public static HashMap<String, String> jsonObjectToStringHashMap(JSONObject jsonObject) {

        if(jsonObject.length() == 0) {
            return new HashMap<String, String>();
        }
        HashMap<String, String> valueByKey = new HashMap<String, String>();
        for(int index = 0; index<jsonObject.names().length(); index++) {
            String key;
            try {
                key = jsonObject.names().getString(index);
                String value = jsonObject.get(jsonObject.names().getString(index)).toString();
                valueByKey.put(key, value);
            } catch (JSONException e) {
                //handle error
                return valueByKey;
            }
        }
        return valueByKey;
    }

    public static HashMap<Long, String> jsonObjectToLongMap(JSONObject jsonObject) {

        if(jsonObject.length() == 0) {
            return new HashMap<Long, String>();
        }
        HashMap<Long, String> valueByKey = new HashMap<Long, String>();
        for(int index = 0; index<jsonObject.names().length(); index++) {
            Long key;
            try {
                key = Long.valueOf(jsonObject.names().getString(index));
                String value = jsonObject.get(jsonObject.names().getString(index).toString()).toString();
                valueByKey.put(key, value);
            } catch (JSONException e) {
                //handle error
                return valueByKey;
            }
        }
        return valueByKey;
    }

}

