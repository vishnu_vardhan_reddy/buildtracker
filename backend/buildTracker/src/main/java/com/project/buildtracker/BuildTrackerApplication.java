package com.project.buildtracker;

import java.util.Map;

import org.skife.jdbi.v2.DBI;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.project.buildtracker.resources.AdminWebResource;

import io.dropwizard.Application;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;

public class BuildTrackerApplication extends Application<BuildTrackerConfiguration> {
    
    @Override
	public String getName() {
		return "buildTracker";
	}

	@Override
	public void run(BuildTrackerConfiguration configuration, Environment environment) throws Exception {
		final DBIFactory factory = new DBIFactory();
		final DBI jdbi = factory.build(environment, configuration.getDataSourceFactory(), "mysql");
		Injector injector = Guice.createInjector(new BuildTrackerModule(jdbi));

		environment.jersey().register(injector.getInstance(AdminWebResource.class));
	}

	public void initialize(Bootstrap<BuildTrackerConfiguration> bootstrap) {
		bootstrap.addBundle(new ViewBundle<BuildTrackerConfiguration>() {
			@Override
			public Map<String, Map<String, String>> getViewConfiguration(BuildTrackerConfiguration config) {
				return config.getViewRendererConfiguration();
			}
		});
	}


	public static void main(String [] args) throws Exception {
		new BuildTrackerApplication().run(args);
	}

}
