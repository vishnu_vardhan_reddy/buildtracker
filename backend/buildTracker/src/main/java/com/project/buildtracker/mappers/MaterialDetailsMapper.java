package com.project.buildtracker.mappers;

import java.sql.ResultSet;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.project.buildtracker.models.MaterialDetails;

	public class MaterialDetailsMapper implements ResultSetMapper<MaterialDetails>{ 
		public MaterialDetails map(int index, ResultSet rs, StatementContext ctx) {
			try {
				return new MaterialDetails(rs.getString("material_id"),rs.getString("barcode"),rs.getString("code"),rs.getString("family_and_type"),
						rs.getString("area"),rs.getString("cost"),rs.getString("material_order_date"),rs.getString("material_order_status"),rs.getString("material_delivery_date"),
						rs.getString("material_delivery_status"),rs.getString("material_location"),rs.getString("item_cost"));
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			} 
			
		}
	}
