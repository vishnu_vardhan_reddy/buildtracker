package com.project.buildtracker.db;

import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import com.project.buildtracker.mappers.QuestionsVerbalMapper;
import com.project.buildtracker.models.QuestionsVerbal;


@RegisterMapper(QuestionsVerbalMapper.class)
public interface QuestionsVerbalDAO {
	
	@SqlQuery("select * from questions_verbal where question_id='123'")
	QuestionsVerbal findQuestionById();

}
