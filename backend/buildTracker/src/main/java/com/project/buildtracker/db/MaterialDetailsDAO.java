package com.project.buildtracker.db;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import com.project.buildtracker.mappers.MaterialDetailsMapper;
import com.project.buildtracker.models.MaterialDetails;

@RegisterMapper(MaterialDetailsMapper.class)
public interface MaterialDetailsDAO {
	
	
	//barcode,code,familyAndType,materialOrderDate,materialOrderStatus,materialDeliveryDate,materialDeliveryStatus,materialLocation,itemCost
	//insert query
	@SqlUpdate("insert into material_details (material_id,barcode,code,family_and_type,area,cost,material_order_date,material_order_status,"
			+ "material_delivery_date,material_delivery_status,material_location,item_cost) "
			+ "values (:materialId,:barcode,:code,:familyAndType,:area,:cost,:materialOrderDate,:materialOrderStatus,:materialDeliveryDate,"
			+ ":materialDeliveryStatus,:materialLocation,:itemCost)")
	int insertIntoMaterialDetails(@Bind("materialId") String materialId, @Bind("barcode") String barcode, @Bind("code") String code, 
			@Bind("familyAndType") String familyAndType, @Bind("area") String area, @Bind("cost") String cost,
			@Bind("materialOrderDate") String materialOrderDate, @Bind("materialOrderStatus") String materialOrderStatus,
			@Bind("materialDeliveryDate") String materialDeliveryDate, @Bind("materialDeliveryStatus") String materialDeliveryStatus, 
			@Bind("materialLocation") String materialLocation, @Bind("itemCost") String itemCost );
	
	
	@SqlQuery("select * from material_details md, project_details pd, project_material_mapping mpd where md.material_id = mpd.material_id "
			+ "and pd.project_id = mpd.project_id and pd.project_id=:projectId")
	MaterialDetails getMaterialDetailsFromProjectId(@Bind("projectId") String projectId);
	
	@SqlQuery("select * from material_details where material_id=:materialId")
	MaterialDetails getMaterialDetails(@Bind("materialId") String materialId);
	
	
	@SqlUpdate("update material_details set family_and_type=:familyAndType,area=:area,cost=:cost,"
			+ "material_order_date=:materialOrderDate,material_order_status=:materialOrderStatus,"
			+ "material_delivery_date=:materialDeliveryDate,material_delivery_status=:materialDeliveryStatus,material_location=:materialLocation,"
			+ "item_cost=:itemCost where material_id=:materialId")
	int updateMaterialDetailsForId(@Bind("materialId") String materialId, 
			@Bind("familyAndType") String familyAndType, @Bind("area") String area, @Bind("cost") String cost,
			@Bind("materialOrderDate") String materialOrderDate, @Bind("materialOrderStatus") String materialOrderStatus,
			@Bind("materialDeliveryDate") String materialDeliveryDate, @Bind("materialDeliveryStatus") String materialDeliveryStatus, 
			@Bind("materialLocation") String materialLocation, @Bind("itemCost") String itemCost);
}
