package com.project.buildtracker.models;

import javax.persistence.Column;

public class QuestionsVerbal {

	@Column(name = "question_id")
	private String questionId;
	
	@Column(name = "topic")
	private String topic;
	
	@Column(name = "subtopic")
	private String subtopic;
	
	@Column(name = "difficulty_level")
	private String difficultyLevel;
	
	@Column(name = "question_type")
	private String questionType;
	
	@Column(name = "question_content")
	private String questionContent;
	
	@Column(name = "question_choices")
	private String questionChoices;
	
	@Column(name = "answer_content")
	private String answerContent;
	
	@Column(name = "tags")
	private String tags;
	
	@Column(name = "created_by")
	private String createdBy;
	
	@Column(name = "creation_date")
	private String creationDate;
	
	
	public QuestionsVerbal(String questionId, String topic, String subtopic, String difficultyLevel, String questionType,
			String questionContent, String questionChoices, String answerContent, String tags, String createdBy, String creationDate) {
		this.questionId = questionId;
		this.topic = topic;
		this.subtopic = subtopic;
		this.difficultyLevel = difficultyLevel;
		this.questionType = questionType;
		this.questionContent = questionContent;
		this.questionChoices = questionChoices;
		this.answerContent = answerContent;
		this.tags = tags;
		this.createdBy = createdBy;
		this.creationDate = creationDate;
	}


	public String getQuestionId() {
		return questionId;
	}


//	public void setQuestionId(String questionId) {
//		this.questionId = questionId;
//	}
	
	public void setQuestionId(String someId) {
		questionId = someId;
	}

	public String getTopic() {
		return topic;
	}


	public void setTopic(String topic) {
		this.topic = topic;
	}


	public String getSubtopic() {
		return subtopic;
	}


	public void setSubtopic(String subtopic) {
		this.subtopic = subtopic;
	}


	public String getDifficultyLevel() {
		return difficultyLevel;
	}


	public void setDifficultyLevel(String difficultyLevel) {
		this.difficultyLevel = difficultyLevel;
	}


	public String getQuestionType() {
		return questionType;
	}


	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}


	public String getQuestionContent() {
		return questionContent;
	}


	public void setQuestionContent(String questionContent) {
		this.questionContent = questionContent;
	}


	public String getQuestionChoices() {
		return questionChoices;
	}


	public void setQuestionChoices(String questionChoices) {
		this.questionChoices = questionChoices;
	}


	public String getAnswerContent() {
		return answerContent;
	}


	public void setAnswerContent(String answerContent) {
		this.answerContent = answerContent;
	}


	public String getTags() {
		return tags;
	}


	public void setTags(String tags) {
		this.tags = tags;
	}


	public String getCreatedBy() {
		return createdBy;
	}


	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}


	public String getCreationDate() {
		return creationDate;
	}


	public void setCreationDate(String creationDate) {
		this.creationDate = creationDate;
	}
     
     
}
