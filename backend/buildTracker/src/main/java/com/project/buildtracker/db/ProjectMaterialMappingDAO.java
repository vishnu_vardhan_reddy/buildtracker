package com.project.buildtracker.db;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import com.project.buildtracker.mappers.ProjectMaterialMappingMapper;
import com.project.buildtracker.models.ProjectMaterialMapping;

@RegisterMapper(ProjectMaterialMappingMapper.class)
public interface ProjectMaterialMappingDAO {
	
	@SqlUpdate("insert into project_material_mapping (project_id,material_id) values (:projectId,:materialId)")
	int insertProjectMaterialMapping(@Bind("projectId") String projectId, @Bind("materialId") String materialId);
	
}