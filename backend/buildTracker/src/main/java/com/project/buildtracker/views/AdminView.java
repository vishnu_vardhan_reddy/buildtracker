package com.project.buildtracker.views;

import java.util.HashMap;
import java.util.Map;

import com.project.buildtracker.models.QuestionsVerbal;

import io.dropwizard.views.View;

public class AdminView extends View {
	
	QuestionsVerbal questionsVerbal;
	Map<String, Object> valueByKey;
	
	public enum Template {
        WEBADMINMAIN("freemarker/admin.ftl"),
        UPLOADTEXTFILE("freemarker/uploading_page.ftl"),
		WEBDISPLAYQUESTIONS("freemarker/display_questions.ftl"),
		UPLOADFILEPOST("freemarker/upload_file_post.ftl"),
		DISPLAYQR("freemarker/display_qr.ftl");

        private String templateName;

        Template(String templateName) {
            this.templateName = templateName;
        }

        public String getTemplateName() {
            return templateName;
        }
    }

    public AdminView(AdminView.Template template) {
        super(template.getTemplateName());
    }
    
    
    
    public AdminView(AdminView.Template template, QuestionsVerbal questionsVerbal) {
        super(template.getTemplateName());
    	this.questionsVerbal = questionsVerbal;
    }
    
    public AdminView(AdminView.Template template, Map<String,Object> valueByKey) {
        super(template.getTemplateName());
        this.valueByKey = valueByKey;
    }
    
    public Map<String,Object> getValueByKey() {
    	return this.valueByKey;
    }
}
