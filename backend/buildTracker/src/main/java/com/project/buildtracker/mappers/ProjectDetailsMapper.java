package com.project.buildtracker.mappers;

import java.sql.ResultSet;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.project.buildtracker.models.ProjectDetails;

public class ProjectDetailsMapper implements ResultSetMapper<ProjectDetails>{
	public ProjectDetails map(int index, ResultSet rs, StatementContext ctx) {
		try {
			/* alternative way
			ProjectDetails projectDetails = new ProjectDetails();
			projectDetails.setProjectId(rs.getInt("project_id"));
			projectDetails.setCreatedAt(rs.getString("created_at"));*/
			
			
			return new ProjectDetails(rs.getString("project_id"),rs.getString("location"), rs.getString("projectName"), 
					rs.getString("uploaded_by"), rs.getString("last_updated"), rs.getString("created_at"), 
					rs.getString("projectStatus"));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	
	
}
