package com.project.buildtracker.models;

import javax.persistence.Column;
public class ProjectMaterialMapping {
	//contains all columns in table.
	@Column(name = "project_id")
	private String projectId;
	
	@Column(name = "material_id")
	private String materialId;
	
	@Column(name = "last_updated")
	private String lastUpdated;
	
	@Column(name = "created_at")
	private String createdAt;
	
	public ProjectMaterialMapping() {
		
	}
	
	public ProjectMaterialMapping( String projectId,String materialId, 
			String lastUpdated, String createdAt) {
		this.projectId= projectId;
		this.materialId = materialId;
		this.lastUpdated = lastUpdated;
		this.createdAt = createdAt;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getMaterialId() {
		return materialId;
	}

	public void setMaterialId(String materialId) {
		this.materialId = materialId;
	}

	public String getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}


}