package com.project.buildtracker.mappers;

import java.sql.ResultSet;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.project.buildtracker.models.ProjectMaterialMapping;

public class ProjectMaterialMappingMapper implements ResultSetMapper<ProjectMaterialMapping>{
	public ProjectMaterialMapping map(int index, ResultSet rs, StatementContext ctx) {
		try {	
			return new ProjectMaterialMapping(rs.getString("project_id"), rs.getString("material_id"),
					rs.getString("last_updated"), rs.getString("created_at"));
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
}