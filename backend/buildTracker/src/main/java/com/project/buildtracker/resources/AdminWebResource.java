package com.project.buildtracker.resources;
import static com.google.common.collect.Maps.newHashMap;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

import com.google.inject.Inject;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.project.buildtracker.db.MaterialDetailsDAO;
import com.project.buildtracker.db.ProjectDetailsDAO;
import com.project.buildtracker.db.ProjectMaterialMappingDAO;
import com.project.buildtracker.models.MaterialDetails;
import com.project.buildtracker.views.AdminView;


@Path("/v1/web/admin")
public class AdminWebResource {
	
	private MaterialDetailsDAO materialDetailsDAO;
	private ProjectDetailsDAO projectDetailsDAO;
	private ProjectMaterialMappingDAO projectMaterialMappingDAO;
	
	@Inject
	public AdminWebResource (MaterialDetailsDAO materialDetailsDAO,ProjectMaterialMappingDAO projectMaterialMappingDAO,
			ProjectDetailsDAO projectDetailsDAO) {
		this.materialDetailsDAO = materialDetailsDAO;
		this.projectDetailsDAO = projectDetailsDAO;
		this.projectMaterialMappingDAO = projectMaterialMappingDAO;
	}
	
	
	@GET
    @Path("/browse")
    @Produces(MediaType.TEXT_HTML)
    public AdminView getPersonViewMustache() {
        return new AdminView(AdminView.Template.WEBADMINMAIN);
    }

	@GET
    @Path("/upload-file")
    @Produces(MediaType.TEXT_HTML)
    public AdminView uploadRevitTextFile() {
        // no need of logic to open the page
		return new AdminView(AdminView.Template.UPLOADTEXTFILE);
    }
	
	
	//TODO - take file from the .../upload-file url - .5 hours
	//TODO - url to ....??
	//TODO - show barcodes for all the lines of code - 2 hours 
	//insert into project_details and material_project_mapping - 2 hours
	// display all barcodes for one project id. project id - url timestamp - .5 hour
	// 5 hours. + android - 2 hours.
	
	
	
	
	@POST
	@Path("/upload-file-post")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.TEXT_HTML)
	public AdminView uploadFilePost(){
		//change it to getting the file from /upload-file
		String csvFile = "/Users/vishnu/Desktop/4D Simulation for Material Management using Shared Parameters.csv";
		
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		Map<String,Object> valueByKey = newHashMap(); 
		//create project
		String projectId = String.valueOf(System.currentTimeMillis());
		int didInsertIntoProject = projectDetailsDAO.insertProjectDetail(projectId);
		if (didInsertIntoProject < 1) {
			//error
			valueByKey.put("status","failure at creating a new project");
			return new AdminView(AdminView.Template.DISPLAYQR,valueByKey);
		}
		
		try {
			br = new BufferedReader(new FileReader(csvFile));
			boolean shouldReadValues = false; // stays false till we reach 'Item cost'
			String materialId = "";
			//count for the data
			
			while ((line = br.readLine()) != null) {

				// use comma as separator
				//need to check if the values are right.
				//no need of setting the values.
				if (shouldReadValues) {
					String[] values = line.split(cvsSplitBy);				
					
					//copy to database. 
					//order - barcode,code,familyAndType,materialOrderDate,materialOrderStatus,materialDeliveryDate,materialDeliveryStatus,materialLocation,itemCost
					materialId = String.valueOf(System.currentTimeMillis());
					int didInsertRow = materialDetailsDAO.insertIntoMaterialDetails(materialId,values[0],values[1],values[2],values[3],values[4],values[5],values[6],values[7],
							values[8],values[9],values[10]);
					//TODO - fix this. overwrites the previous image
					//String url = "localhost:8080/v1/web/admin/materials/"+materialId;
					String url = "192.168.43.83:8080/v1/web/admin/materials/"+materialId;
					generateQRCode(url, materialId, valueByKey);
					
					if (didInsertRow < 1) {
						valueByKey.put("status","failure at creating a new material_details row");
						return new AdminView(AdminView.Template.DISPLAYQR,valueByKey);
					}
					
					int didInsertInMappingTable = projectMaterialMappingDAO.insertProjectMaterialMapping(projectId,materialId);
					if (didInsertInMappingTable < 1) {
						valueByKey.put("status","failure at creating a new project material mapping");
						return new AdminView(AdminView.Template.DISPLAYQR,valueByKey);
					}
				}
				
				//start reading the values after the first line
				shouldReadValues = true;

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		valueByKey.put("status","success");
		return new AdminView(AdminView.Template.DISPLAYQR,valueByKey); 
	}
	
	
	@GET
	@Path("/materials/{materialId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPersonViewMustache(@PathParam("materialId") String materialId) {
		Map<String,Object> response = newHashMap();
		
		MaterialDetails materialDetails = materialDetailsDAO.getMaterialDetails(materialId);
		if (materialDetails != null) {
			response.put("materialDetails", materialDetails);
			response.put("status", "SUCCESS");
		} else {
			response.put("status", "FAILURE");
			response.put("message", "No such file exists");
		}
		
		return Response.ok(response).build();
	}
	
	
	
	@PUT
	@Path("/materials/{materialId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateMaterialDetails(@PathParam("materialId") String materialId, MultivaluedMap<String,String> params) {
		System.out.println("......................................"+params);
		
		Map<String,Object> response = newHashMap();
		int didUpdateDetails = materialDetailsDAO.updateMaterialDetailsForId(materialId, 
				params.get("familyAndType").get(0), params.get("area").get(0), params.get("cost").get(0),params.get("materialOrderDate").get(0),
				params.get("materialOrderStatus").get(0),
				params.get("materialDeliveryDate").get(0), params.get("materialDeliveryStatus").get(0),params.get("materialLocation").get(0) , 
				params.get("itemCost").get(0));
		
		if (didUpdateDetails > 0) {
			response.put("status", "success - material details saved");
		} else {
			response.put("status", "failure - couldn't save material details");
		}
		return Response.ok(response).build();
	}
	
	
	
	//didnt use the method yet. need to do so.
	//do it. i should be able to hit api and get some response. empty response is also fine.
	//add a skeleton. okay
	
	
	
	//creating a qr code
	public void generateQRCode(String qrCodeUrl, String materialId, Map<String,Object> valueByKey) {
        
        // change path as per your laptop/desktop location
        String filePath = "/Users/vishnu/Downloads/"+materialId+".png";
        int size = 125;
        String fileType = "png";
        File myFile = new File(filePath);
        try {
            Hashtable<EncodeHintType, ErrorCorrectionLevel> hintMap = new Hashtable<EncodeHintType, ErrorCorrectionLevel>();
            hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix byteMatrix = qrCodeWriter.encode(qrCodeUrl,BarcodeFormat.QR_CODE, size, size, hintMap);
            int CrunchifyWidth = byteMatrix.getWidth();
            BufferedImage image = new BufferedImage(CrunchifyWidth, CrunchifyWidth,
                    BufferedImage.TYPE_INT_RGB);
            image.createGraphics();
 
            Graphics2D graphics = (Graphics2D) image.getGraphics();
            graphics.setColor(Color.WHITE);
            graphics.fillRect(0, 0, CrunchifyWidth, CrunchifyWidth);
            graphics.setColor(Color.BLACK);
 
            for (int i = 0; i < CrunchifyWidth; i++) {
                for (int j = 0; j < CrunchifyWidth; j++) {
                    if (byteMatrix.get(i, j)) {
                        graphics.fillRect(i, j, 1, 1);
                    }
                }
            }
            ImageIO.write(image, fileType, myFile);
            //wait
            byte[] imgBytes = IOUtils.toByteArray(new FileInputStream(myFile));
            byte[] imgBytesAsBase64 = Base64.encodeBase64(imgBytes);
            String imgDataAsBase64 = new String(imgBytesAsBase64);
            String imgAsBase64 = "data:image/png;base64," + imgDataAsBase64;
            System.out.println("------------------------------------------");
            System.out.println(imgAsBase64);
            System.out.println("------------------------------------------");
            
            valueByKey.put(materialId, imgAsBase64);
            
            
        } catch (WriterException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }       
}