package com.project.buildtracker.db;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

import com.project.buildtracker.mappers.ProjectDetailsMapper;

@RegisterMapper(ProjectDetailsMapper.class)
public interface ProjectDetailsDAO {
	
	@SqlUpdate("insert into project_details (project_id) values (:projectId)")
	int insertProjectDetail(@Bind("projectId") String projectId);
}
