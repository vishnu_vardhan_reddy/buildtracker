package com.project.buildtracker.models;

import javax.persistence.Column;

public class MaterialDetails {
	//contains all columns in table.
	@Column(name = "material_id")
	private String materialId;
	
	@Column(name = "barcode")
	private String barcode;
	
	@Column(name = "code")
	private String code;
	@Column(name = "family_and_type")
	private String familyAndType;
	
	@Column(name = "area")
	private String area;
	
	@Column(name = "cost")
	private String cost;
	
	@Column(name = "material_order_date")
	private String materialOrderDate;

	@Column(name = "material_order_status")
	private String materialOrderStatus;
	
	@Column(name = "material_delivery_date")
	private String materialDeliveryDate;
	
	@Column(name = "material_delivery_status")
	private String materialDeliveryStatus;
	
	@Column(name = "material_location")
	private String materialLocation;
	
	@Column(name = "item_cost")
	private String itemCost;
	
	public MaterialDetails() {
		
	}
	
	public MaterialDetails(String materialId,String barcode, String code, String familyAndType, String area, String cost, 
			String materialOrderDate,String materialOrderStatus,String materialDeliveryDate,String materialDeliveryStatus,
			String materialLocation,String itemCost) {
		this.materialId = materialId;
		this.barcode= barcode;
		this. code =  code;
		this.familyAndType = familyAndType;
		this.area = area;
		this.cost = cost;
		this.materialOrderDate = materialOrderDate;
		this.materialOrderStatus = materialOrderStatus;
		this.materialDeliveryDate = materialDeliveryDate;
		this.materialDeliveryStatus = materialDeliveryStatus;
		this.materialLocation = materialLocation;
		this.itemCost = itemCost;
	}

	public String getMaterialId() {
		return materialId;
	}

	public void setMaterialId(String materialId) {
		this.materialId = materialId;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getFamilyAndType() {
		return familyAndType;
	}

	public void setFamilyAndType(String familyAndType) {
		this.familyAndType = familyAndType;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCost() {
		return cost;
	}

	public void setCost(String cost) {
		this.cost = cost;
	}

	public String getMaterialOrderDate() {
		return materialOrderDate;
	}

	public void setMaterialOrderDate(String materialOrderDate) {
		this.materialOrderDate = materialOrderDate;
	}

	public String getMaterialOrderStatus() {
		return materialOrderStatus;
	}

	public void setMaterialOrderStatus(String materialOrderStatus) {
		this.materialOrderStatus = materialOrderStatus;
	}

	public String getMaterialDeliveryDate() {
		return materialDeliveryDate;
	}

	public void setMaterialDeliveryDate(String materialDeliveryDate) {
		this.materialDeliveryDate = materialDeliveryDate;
	}

	public String getMaterialDeliveryStatus() {
		return materialDeliveryStatus;
	}

	public void setMaterialDeliveryStatus(String materialDeliveryStatus) {
		this.materialDeliveryStatus = materialDeliveryStatus;
	}

	public String getMaterialLocation() {
		return materialLocation;
	}

	public void setMaterialLocation(String materialLocation) {
		this.materialLocation = materialLocation;
	}

	public String getItemCost() {
		return itemCost;
	}

	public void setItemCost(String itemCost) {
		this.itemCost = itemCost;
	}
}
