package com.project.buildtracker;

import org.skife.jdbi.v2.DBI;

import com.google.inject.AbstractModule;
import com.project.buildtracker.db.MaterialDetailsDAO;
import com.project.buildtracker.db.ProjectDetailsDAO;
import com.project.buildtracker.db.ProjectMaterialMappingDAO;
import com.project.buildtracker.db.QuestionsVerbalDAO;

public class BuildTrackerModule extends AbstractModule {
	private DBI jdbi;
	public BuildTrackerModule(DBI jdbi) {
		this.jdbi = jdbi;
	}
	
	@Override
	protected void configure() {
		final QuestionsVerbalDAO questionsVerbalDAO = jdbi.onDemand(QuestionsVerbalDAO.class);
		final MaterialDetailsDAO materialDetailsDAO = jdbi.onDemand(MaterialDetailsDAO.class);
		final ProjectDetailsDAO projectDetailsDAO = jdbi.onDemand(ProjectDetailsDAO.class);
		final ProjectMaterialMappingDAO projectMaterialMappingDAO = jdbi.onDemand(ProjectMaterialMappingDAO.class);
		
		bind(QuestionsVerbalDAO.class).toInstance(questionsVerbalDAO);
		bind(MaterialDetailsDAO.class).toInstance(materialDetailsDAO);
		bind(ProjectDetailsDAO.class).toInstance(projectDetailsDAO);
		bind(ProjectMaterialMappingDAO.class).toInstance(projectMaterialMappingDAO);
	}
}
