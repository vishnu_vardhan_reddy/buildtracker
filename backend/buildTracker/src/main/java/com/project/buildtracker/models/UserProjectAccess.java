package com.project.buildtracker.models;

import javax.persistence.Column;

public class UserProjectAccess {
	//contains all columns in table.
	@Column(name = "user_id")
	private Integer userId;
	
	@Column(name = "project_id")
	private Integer projectId;
	
	@Column(name = "read_access")
	private Integer readAccess;
	
	@Column(name = "write_access")
	private Integer writeAccess;
	
	@Column(name = "delete_access")
	private Integer deleteAccess;
	
	public UserProjectAccess() {
		
	}
	
	public UserProjectAccess(Integer userId,Integer projectId, Integer readAccess ,Integer writeAccess,Integer deleteAccess) {
		this.userId = userId;
		this.projectId = projectId;
		this.readAccess =  readAccess;
		this.writeAccess = writeAccess;
		this.deleteAccess = deleteAccess;
		
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public Integer getReadAccess() {
		return readAccess;
	}

	public void setReadAccess(Integer readAccess) {
		this.readAccess = readAccess;
	}

	public Integer getWriteAccess() {
		return writeAccess;
	}

	public void setWriteAccess(Integer writeAccess) {
		this.writeAccess = writeAccess;
	}

	public Integer getDeleteAccess() {
		return deleteAccess;
	}

	public void setDeleteAccess(Integer deleteAccess) {
		this.deleteAccess = deleteAccess;
	}

	}