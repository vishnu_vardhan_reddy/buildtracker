package com.project.buildtracker.models;

import javax.persistence.Column;

public class UserDetails {
	//contains all columns in table.
	@Column(name = "user_id")
	private Integer userId;
	
	@Column(name = "user_type")
	private String userType;
	
	@Column(name = "user_name")
	private String userName;
	
	@Column(name = "user_company")
	private String userCompany;
	
	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserCompany() {
		return userCompany;
	}

	public void setUserCompany(String userCompany) {
		this.userCompany = userCompany;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	@Column(name = "created_at")
	private String createdAt;
	
	public UserDetails() {
		
	}
	
	public UserDetails(Integer userId,String userType, String userName, String userCompany,String createdAt) {
		this.userId = userId;
		this.userType= userType;
		this.userName = userName;
		this.userCompany = userCompany;
		this.createdAt = createdAt;
	}

	

}