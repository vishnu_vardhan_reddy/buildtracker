package com.project.buildtracker.models;

import javax.persistence.Column;

public class ProjectDetails {
	//contains all columns in table.
	@Column(name = "project_id")
	private String projectId;
	
	@Column(name = "location")
	private String location;
	
	@Column(name = "project_name")
	private String projectName;
	
	@Column(name = "uploaded_by")
	private String uploadedBy;
	
	@Column(name = "last_updated")
	private String lastUpdated;
	
	@Column(name = "created_at")
	private String createdAt;
	
	@Column(name = "project_status")
	private String projectStatus;
	
	public ProjectDetails() {
		
	}
	
	public ProjectDetails(String projectId,String location, String projectName, String uploadedBy, 
			String lastUpdated, String createdAt, String projectStatus) {
		this.projectId = projectId;
		this.location = location;
		this.projectName = projectName;
		this.uploadedBy = uploadedBy;
		this.createdAt = createdAt;
		this.projectStatus = projectStatus;
		this.lastUpdated = lastUpdated;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getUploadedBy() {
		return uploadedBy;
	}

	public void setUploadedBy(String uploadedBy) {
		this.uploadedBy = uploadedBy;
	}

	public String getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(String lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

	public String getProjectStatus() {
		return projectStatus;
	}

	public void setProjectStatus(String projectStatus) {
		this.projectStatus = projectStatus;
	}
	
}
