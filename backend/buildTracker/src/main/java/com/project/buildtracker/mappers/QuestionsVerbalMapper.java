package com.project.buildtracker.mappers;

import java.sql.ResultSet;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import com.project.buildtracker.models.QuestionsVerbal;

public class QuestionsVerbalMapper implements ResultSetMapper<QuestionsVerbal>{
	public QuestionsVerbal map(int index, ResultSet rs, StatementContext ctx) {
		try {
			return new QuestionsVerbal(rs.getString("question_id"), 
					rs.getString("topic"), 
					rs.getString("subtopic"), 
					rs.getString("difficulty_level"), 
					rs.getString("question_type"),
					rs.getString("question_content"), 
					rs.getString("question_choices"), 
					rs.getString("answer_content"), 
					rs.getString("tags"), 
					rs.getString("created_by"), 
					rs.getString("creation_date"));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
}
