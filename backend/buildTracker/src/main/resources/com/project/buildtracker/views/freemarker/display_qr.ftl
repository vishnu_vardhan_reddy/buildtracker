<html>
  <head></head>
  <body>
    <div>Your QR codes:</div>
    <div>
      <#assign keys = valueByKey?keys>
      <#list keys as key>
        ${key} = <img src="${valueByKey[key]}" /> 
      </#list>
    </div>
  </body>
</html>