<html>
 <head>
   <title>Ipload file</title>
 </head>
 <body>
   <script type="text/javascript">
      $(document).ready(function() {
        
        $('#uploadFile').submit( function(e) {
          var fileName = $("#materialsFile").val();
          if (!(fileName.lastIndexOf("csv")===fileName.length-3)) {
            $('#registererror').html('');
		    $('#registererror').css('display','block');
		    $('#registererror').html("Please check your file");
            return false;
          }	
          return true;
        });
        
      });
    </script>
 
 
 
  <div style="padding-left:100px">
    <h3>Upload a '4D Simulation for Material Management using Shared Parameters.csv' to start tracking your process</h3>
    
    <form action="/v1/web/admin/upload-file-post" encType="multiPart/form-data" method="post" id="uploadFile">
      <div style="color:red" id="registererror"></div>
      <div>Select a file</div>
      <input type="file" name="dataFile" size="50" id="materialsFile" name="csvFile">
      <div><input type="submit" value="Upload file" id="submitFile"></div>
    </form>
  </div>

 </body>
</html>